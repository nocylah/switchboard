use crate::identity::{Domain, DnsAddr};

#[derive(Clone)]
pub struct Config;

impl Config {
    pub fn homeserver_fqdn(&self) -> Domain {
        Domain::DnsAddr(DnsAddr { hostname: "local.town".to_string(), port: None })
    }
}

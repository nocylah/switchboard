use warp::{Filter, Reply, reject::Rejection};
use serde::{Serialize, Deserialize};
use sqlx::SqlitePool;

use crate::db;
use crate::api::client_server::r0_6_1::error::{ErrorCode, api_error};
use crate::api::client_server::r0_6_1::login;
use crate::api::client_server::r0_6_1::api_prefix;
use crate::api::client_server::r0_6_1::auth::auth_check;

#[derive(Deserialize)]
enum AccountKind {
    #[serde(rename = "user")]
    User,
    #[serde(rename = "guest")]
    Guest
}

#[derive(Deserialize)]
struct AuthenticationData {
    r#type: login::LoginType,
    session: String,
}

#[derive(Deserialize)]
struct RegisterRequest {
    kind: AccountKind,
    auth: AuthenticationData,
    username: Option<String>,
    password: Option<String>,
    device_id: Option<String>,
    initial_device_display_name: Option<String>,
    inhibit_login: Option<bool>,
}

#[derive(Serialize)]
struct RegisterReply {
    user_id: String,
    access_token: Option<String>,
    home_server: String,
    device_id: String,
}

pub(crate) fn post_register(pool: &SqlitePool) -> impl Filter<Extract=(impl Reply,), Error=Rejection> + Clone {
    pub async fn handler(device: db::Device, pool: SqlitePool) -> Result<impl warp::Reply, Rejection> {
        device.delete(&pool).await; Ok("{}")
    }

    panic!();

    warp::post()
        .and(api_prefix())
        .and(warp::path!("register"))
        .and(auth_check(pool))
        .map(|device: db::Device| { device.device_id })
}

pub fn routes(pool: &sqlx::SqlitePool) -> impl Filter<Extract=(impl Reply,), Error=Rejection> + Clone {
    post_register(pool)
}

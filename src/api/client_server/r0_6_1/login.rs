use warp::{Filter, Reply, reject::Rejection};
use serde::{Serialize, Deserialize};
use sqlx::SqlitePool;

use crate::config::Config;
use crate::db;
use crate::api::client_server::r0_6_1::error::{ErrorCode, api_error, StdErrHandler};
use crate::api::client_server::r0_6_1::api_prefix;
use crate::api::client_server::r0_6_1::auth::auth_check;
use crate::identity::{MatrixId, FullId, LocalId};

#[derive(PartialEq,Debug)]
#[derive(Deserialize, Serialize)]
pub(crate) enum LoginType {
    #[serde(rename = "m.login.password")]
    Password,
    #[serde(rename = "m.login.recaptcha")]
    ReCaptcha,
    #[serde(rename = "m.login.oauth2")]
    OAuth2,
    #[serde(rename = "m.login.sso")]
    Sso,
    #[serde(rename = "m.login.email.identity")]
    EmailIdentity,
    #[serde(rename = "m.login.msisdn")]
    Msisdn,
    #[serde(rename = "m.login.token")]
    Token,
    #[serde(rename = "m.login.dummy")]
    Dummy
}

#[derive(Deserialize, Serialize)]
pub(crate) struct LoginFlow {
    r#type: LoginType
}

#[derive(Deserialize, Serialize)]
pub(crate) struct LoginInfoReply {
    flows: Vec<LoginFlow>,
}

#[derive(Serialize,Deserialize)]
pub(crate) struct IdUser {
    user: MatrixId,
}

#[derive(Serialize,Deserialize)]
pub(crate) struct IdThirdParty {
    medium: String,
    address: String
}

#[derive(Serialize,Deserialize)]
pub(crate) struct IdPhone {
    country: String,
    phone: String
}

#[derive(Serialize,Deserialize)]
#[serde(tag = "type")]
pub(crate) enum UserIdentifier {
    #[serde(rename = "m.id.user")]
    User(IdUser),
    #[serde(rename = "m.id.thirdparty")]
    ThirdParty(IdThirdParty),
    #[serde(rename = "m.id.phone")]
    Phone(IdPhone)
}

#[derive(Serialize,Deserialize)]
pub(crate) struct LoginRequest {
    r#type: LoginType,
    identifier: Option<UserIdentifier>,
    user: Option<MatrixId>,
    medium: Option<String>,
    address: Option<String>,
    password: Option<String>,
    token: Option<String>,
    device_id: Option<String>,
    initial_device_display_name: Option<String>
}

#[derive(Serialize,Deserialize)]
pub(crate) struct ServerInformation {
    base_url: String
}

#[derive(Serialize,Deserialize)]
pub(crate) struct DiscoveryInformation {
    #[serde(rename = "m.homeserver")]
    home_server: ServerInformation,
    #[serde(rename = "m.identity_server")]
    identity_server: Option<ServerInformation>,
}

#[derive(Serialize,Deserialize)]
pub(crate) struct LoginReply {
    #[serde(with = "serde_with::rust::display_fromstr")]
    user_id: FullId,
    access_token: String,
    home_server: String,
    device_id: String,
    well_known: Option<DiscoveryInformation>
}

pub(crate) fn get_login() -> impl Filter<Extract=(impl Reply,), Error=Rejection> + Clone {
    warp::get()
        .and(api_prefix())
        .and(warp::path!("login"))
        .map(|| {
            let reply = LoginInfoReply {
                flows: vec![LoginFlow{ r#type: LoginType::Password }]
            };
            return warp::reply::json(&reply);
        })
}

pub(crate) fn post_login(config: &Config, pool: &SqlitePool) -> impl Filter<Extract=(impl Reply,), Error=Rejection> + Clone {

    async fn handler(body: LoginRequest, pool: SqlitePool, config: Config) -> Result<Box<dyn warp::Reply>, Rejection> {
        let account = match body.r#type {
            LoginType::Password => {
                let pass = body.password
                    .ok_or(api_error(ErrorCode::BadJson, Some("'password' not provided")))?;

                let ident = match body.identifier {
                    Some(UserIdentifier::User(ident)) => ident,
                    _ => return Err(api_error(ErrorCode::BadJson, Some("'identifier' malformed/missing")))
                };

                let id = match ident.user {
                    MatrixId::Full(mxid) => mxid.local.id,
                    MatrixId::Local(mxid) => mxid.id
                };

                let account = db::Account::find_by_id(&pool, &id).await
                    .std_api_err()?
                    .map_or(Err(api_error(ErrorCode::Forbidden, None)), Ok)?;

                if !account.check_password(&pass) {
                    return Err(api_error(ErrorCode::Forbidden, None));
                }

                account
            }

            _ => return Err(api_error(ErrorCode::BadJson, Some("login type not supported")))
        };

        let device_id = match body.device_id {
            Some(id) => id,
            _ => {
                // TODO: move to util mod
                use rand::Rng;
                let mut rng = rand::thread_rng();
                std::iter::repeat(())
                    .map(|()| rng.sample(rand::distributions::Alphanumeric))
                    .take(8)
                    .collect()
            }
        };

        let device = account.device(&pool, &device_id).await.std_api_err()?;

        let reply = LoginReply {
            user_id: FullId {
                local: LocalId { id: account.user_id },
                domain: config.homeserver_fqdn()
            },
            home_server: String::new(),
            access_token: device.token(),
            device_id: device.device_id,
            well_known: None,
        };

        Ok(Box::new(warp::reply::json(&reply)))
    }

    let pool = pool.clone();
    let config = config.clone();
    warp::post()
        .and(api_prefix())
        .and(warp::path!("login"))
        .and(warp::body::json())
        .and(warp::any().map(move || pool.clone()))
        .and(warp::any().map(move || config.clone()))
        .and_then(handler)
}

pub(crate) fn post_logout(pool: &SqlitePool) -> impl Filter<Extract=(impl Reply,), Error=Rejection> + Clone {
    pub async fn handler(device: db::Device, pool: SqlitePool) -> Result<impl warp::Reply, Rejection> {
        device.delete(&pool).await; Ok("{}")
    }

    let pool = pool.clone();
    warp::post()
        .and(api_prefix())
        .and(warp::path!("logout"))
        .and(auth_check(&pool))
        .and(warp::any().map(move || pool.clone()))
        .and_then(handler)
}

pub(crate) fn post_logout_all(pool: &SqlitePool) -> impl Filter<Extract=(impl Reply,), Error=Rejection> + Clone {
    pub async fn handler(device: db::Device, pool: SqlitePool) -> Result<impl warp::Reply, Rejection> {
        let account = device.account(&pool).await.std_api_err()?;
        for device in account.devices(&pool).await.std_api_err()? {
            device.delete(&pool).await.std_api_err()?;
        }
        Ok("{}")
    }

    let pool = pool.clone();
    warp::post()
        .and(api_prefix())
        .and(warp::path!("logout"))
        .and(auth_check(&pool))
        .and(warp::any().map(move || pool.clone()))
        .and_then(handler)
}

pub(crate) fn routes(config: &Config, pool: &SqlitePool) -> impl Filter<Extract=(impl Reply,), Error=Rejection> + Clone {
    return get_login()
        .or(post_login(config, pool))
        .or(post_logout(pool))
        .or(post_logout_all(pool))
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::super::error::ApiError;
    use crate::identity::{Domain, DnsAddr};

    fn login_request() -> LoginRequest {
        LoginRequest {
            r#type: LoginType::Password,
            identifier: Some(UserIdentifier::User(
                IdUser {
                    user: MatrixId::Full(FullId{
                        local: LocalId { id: "user0".to_string() },
                        domain: Domain::DnsAddr(DnsAddr {
                            hostname: "some.where.gtld".to_string(),
                            port: None
                        })
                    })
                }
            )),
            user: None,
            medium: None,
            address: None,
            password: Some("bad password".to_string()),
            token: None,
            device_id: None,
            initial_device_display_name: None
        }
    }

    fn prefix(method: &str) -> warp::test::RequestBuilder  {
        warp::test::request().method("POST")
            .path(&format!("/_matrix/_client/r0/{}", method))
    }

    #[tokio::test]
    async fn test_get_login() -> Result<(), Box<dyn std::error::Error>> {
        let filter = get_login();

        let res = warp::test::request()
            .method("GET")
            .path("/_matrix/_client/r0/login")
            .reply(&filter)
            .await;

        assert_eq!(res.status(), 200);

        let body: LoginInfoReply = serde_json::from_slice(res.body())?;
        assert_eq!(body.flows.len(), 1);
        assert_eq!(body.flows[0].r#type, LoginType::Password);

        Ok(())
    }

    #[tokio::test]
    async fn test_post_login() -> Result<(), Box<dyn std::error::Error>> {
        let pool = crate::test::fake_db().await;
        let config = crate::config::Config;
        let filter = post_login(&config, &pool);

        // empty body
        let res = prefix("login").filter(&filter).await;
        assert!(res.err().expect("missing rejection").find::<warp::body::BodyDeserializeError>().is_some());

        // no identifier
        let mut req = login_request();
        req.identifier = None;
        let res = prefix("login").json(&req).filter(&filter).await;
        let err = res.err().expect("missing rejection").find::<ApiError>().expect("missing ApiError").clone();
        assert!(err.errcode == ErrorCode::BadJson);

        // no password
        let mut req = login_request();
        req.password = None;
        let res = prefix("login").json(&req).filter(&filter).await;
        let err = res.err().expect("missing rejection").find::<ApiError>().expect("missing ApiError").clone();
        assert!(err.errcode == ErrorCode::BadJson);

        // bad password
        let mut req = login_request();
        req.password = Some("12345678".to_string());
        let res = prefix("login").json(&req).filter(&filter).await;
        let err = res.err().expect("missing rejection").find::<ApiError>().expect("missing ApiError").clone();
        assert!(err.errcode == ErrorCode::Forbidden);

        // password request
        let req = login_request();
        let res = prefix("login").json(&req).reply(&filter).await;
        let reply: LoginReply = serde_json::from_slice(&res.body())?;

        assert_eq!(res.status(), 200);
        assert_eq!(&reply.user_id.local.id, "user0");
        let device = db::Device::find_by_token(&pool, &reply.access_token).await?.expect("no device found");
        assert_eq!(device.device_id, reply.device_id);
        assert_eq!(device.user_id, "user0");

        // specify existing device_id
        let mut req = login_request();
        req.device_id = Some(reply.device_id);
        let res = prefix("login").json(&req).reply(&filter).await;
        let reply: LoginReply = serde_json::from_slice(&res.body())?;

        assert_eq!(res.status(), 200);
        assert_eq!(&reply.user_id.local.id, "user0");
        let device = db::Device::find_by_token(&pool, &reply.access_token).await?.expect("no device found");
        assert_eq!(device.device_id, reply.device_id);
        assert_eq!(device.user_id, "user0");

        // specify new device_id
        let mut req = login_request();
        req.device_id = Some("newid".to_string());
        let res = prefix("login").json(&req).reply(&filter).await;
        let reply: LoginReply = serde_json::from_slice(&res.body())?;

        assert_eq!(res.status(), 200);
        assert_eq!(&reply.user_id.local.id, "user0");
        let device = db::Device::find_by_token(&pool, &reply.access_token).await?.expect("no device found");
        assert_eq!(device.device_id, "newid");
        assert_eq!(device.user_id, "user0");

        Ok(())
    }

    #[tokio::test]
    async fn test_post_logout() -> Result<(), Box<dyn std::error::Error>> {
        let pool = crate::test::fake_db().await;
        let logout_filter = post_logout(&pool);

        // missing token
        let res = prefix("logout").header("Authorization", "bad token")
            .filter(&logout_filter).await;
        let err = res.err().expect("missing rejection").find::<ApiError>().expect("missing ApiError").clone();
        assert!(err.errcode == ErrorCode::UnknownToken);

        // successful logout
        let res = prefix("logout").header("Authorization", "ABCDEFG")
            .reply(&logout_filter).await;
        assert_eq!(res.status(), 200);
        let device = db::Device::find_by_token(&pool, "ABCDEFG").await?;
        assert!(device.is_none());

        Ok(())
    }

    #[tokio::test]
    async fn test_post_logout_all() -> Result<(), Box<dyn std::error::Error>> {
        let pool = crate::test::fake_db().await;
        let logout_filter = post_logout_all(&pool);

        // missing token
        let res = prefix("logout").header("Authorization", "bad token")
            .filter(&logout_filter).await;
        let err = res.err().expect("missing rejection").find::<ApiError>().expect("missing ApiError").clone();
        assert!(err.errcode == ErrorCode::UnknownToken);

        // successful logout
        let acct = db::Account::find_by_id(&pool, "user0").await?.expect("no account found");
        assert!(acct.devices(&pool).await?.len() == 2);

        let res = prefix("logout").header("Authorization", "ABCDEFG")
            .reply(&logout_filter).await;
        assert_eq!(res.status(), 200);
        let device = db::Device::find_by_token(&pool, "ABCDEFG").await?;
        assert!(device.is_none());

        assert!(acct.devices(&pool).await?.len() == 0);

        Ok(())
    }
}

use warp::{Reply, reject::Rejection, reject::Reject, http::StatusCode};
use serde::{Serialize};
use std::convert::Infallible;

#[derive(PartialEq)]
#[derive(Debug, Copy, Clone)]
#[derive(Serialize)]
pub enum ErrorCode {
    #[serde(rename = "M_FORBIDDEN")]
    Forbidden,
    #[serde(rename = "M_UNKNOWN_TOKEN")]
    UnknownToken,
    #[serde(rename = "M_MISSING_TOKEN")]
    MissingToken,
    #[serde(rename = "M_BAD_JSON")]
    BadJson,
    #[serde(rename = "M_NOT_JSON")]
    NotJson,
    #[serde(rename = "M_NOT_FOUND")]
    NotFound,
    #[serde(rename = "M_LIMIT_EXCEEDED")]
    LimitExceeded,
    #[serde(rename = "M_UNKNOWN")]
    Unknown,
    #[serde(rename = "M_UNRECOGNIZED")]
    Unrecognized,
    #[serde(rename = "M_UNAUTHORIZED")]
    Unauthorized,
    #[serde(rename = "M_USER_DEACTIVATED")]
    UserDeactivated,
    #[serde(rename = "M_USER_IN_USE")]
    UserInUse,
    #[serde(rename = "M_INVALID_USERNAME")]
    InvalidUsername,
    #[serde(rename = "M_ROOM_IN_USE")]
    RoomInUse,
    #[serde(rename = "M_INVALID_ROOM_STATE")]
    InvalidRoomState,
    #[serde(rename = "M_THREEPID_IN_USE")]
    ThreepidInUse,
    #[serde(rename = "M_THREEPID_NOT_FOUND")]
    ThreepidNotFound,
    #[serde(rename = "M_THREEPID_AUTH_FAILED")]
    ThreepidAuthFailed,
    #[serde(rename = "M_THREEPID_DENIED")]
    ThreepidDenied,
    #[serde(rename = "M_SERVER_NOT_TRUSTED")]
    ServerNotTrusted,
    #[serde(rename = "M_UNSUPPORTED_ROOM_VERSION")]
    UnsupportedRoomVersion,
    #[serde(rename = "M_INCOMPATIBLE_ROOM_VERSION")]
    IncompatibleRoomVersion,
    #[serde(rename = "M_BAD_STATE")]
    BadState,
    #[serde(rename = "M_GUEST_ACCESS_FORBIDDEN")]
    GuestAccessForbidden,
    #[serde(rename = "M_CAPTCHA_NEEDED")]
    CaptchaNeeded,
    #[serde(rename = "M_CAPTCHA_INVALID")]
    CaptchaInvalid,
    #[serde(rename = "M_MISSING_PARAM")]
    MissingParam,
    #[serde(rename = "M_INVALID_PARAM")]
    InvalidParam,
    #[serde(rename = "M_TOO_LARGE")]
    TooLarge,
    #[serde(rename = "M_EXCLUSIVE")]
    Exclusive,
    #[serde(rename = "M_RESOURCE_LIMIT_EXCEEDED")]
    ResourceLimitExceeded,
    #[serde(rename = "M_CANNOT_LEAVE_SERVER_NOTICE_ROOM")]
    CannotLeaveServerNoticeRoom
}

#[derive(Debug, Clone)]
#[derive(Serialize)]
pub(crate) struct ApiError {
    pub errcode: ErrorCode,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub error: Option<String>
}

impl Reject for ApiError {}

pub fn api_error(err: ErrorCode, reason: Option<&str>) -> Rejection {
    let reply = ApiError { errcode: err, error: reason.map(String::from) };
    warp::reject::custom(reply)
}

pub async fn handle_rejection(err: Rejection) -> Result<impl Reply, Infallible> {
    let mut reply = ApiError {
        errcode: ErrorCode::Unknown,
        error: Some(String::from("internal error"))
    };

    if let Some(api_error) = err.find::<ApiError>() {
        reply = api_error.clone();
    }
    else if let Some(err) = err.find::<warp::body::BodyDeserializeError>() {
        reply = ApiError {
            errcode: ErrorCode::BadJson,
            error: Some(format!("malformed body: {}", err))
        };
    }

    return Ok(warp::reply::with_status(
            warp::reply::json(&reply),
            StatusCode::BAD_REQUEST));
}

pub trait StdErrHandler<A> {
    fn std_api_err(self) -> Result<A, Rejection>;
}

impl<A, B> StdErrHandler<A> for Result<A, B> {
    fn std_api_err(self) -> Result<A, Rejection> {
        match self {
            Ok(value) => Ok(value),
            _ => Err(api_error(ErrorCode::Unknown, Some("internal error")))
        }
    }
}

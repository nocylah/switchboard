mod api;
mod auth;
mod config;
mod db;
mod identity;
mod test;

use sqlx::sqlite::SqlitePool;

#[tokio::main]
async fn main() -> Result<(), sqlx::Error> {
    println!("Hello, world!");

    let config = config::Config;

    let pool = SqlitePool::builder()
        .max_size(5)
        .build("sqlite://db.sqlite3").await?;

    let routes = api::client_server::r0_6_1::routes(&config, &pool);
    let srv = warp::serve(routes);
    srv.run(([127, 0, 0, 1], 3030)).await;

    Ok(())
}

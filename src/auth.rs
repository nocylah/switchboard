pub fn rand_alphanumeric(length: usize) -> String {
    use rand::Rng;

    let mut rng = rand::thread_rng();

    std::iter::repeat(())
        .map(|()| rng.sample(rand::distributions::Alphanumeric))
        .take(length)
        .collect()
}

pub fn gen_token() -> String {
    rand_alphanumeric(256)
}

pub fn hash_password(password: &str) -> String {
    let params = scrypt::ScryptParams::recommended();
    scrypt::scrypt_simple(password, &params).expect("OS RNG should not fail")
}

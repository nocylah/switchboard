CREATE TABLE IF NOT EXISTS room (
    room_id TEXT PRIMARY KEY,
    name TEXT NOT NULL,
    topic TEXT,
    world_readable BOOLEAN NOT NULL,
    guests_can_join BOOLEAN NOT NULL,
    avatar_url TEXT,
    is_local BOOLEAN NOT NULL
);

CREATE TABLE IF NOT EXISTS room_alias (
    alias TEXT PRIMARY KEY,
    room_id TEXT NOT NULL REFERENCES room(room_id)
);

CREATE TABLE IF NOT EXISTS room_member (
    room_id NOT NULL REFERENCES room(room_id),
    user_id NOT NULL REFERENCES user(user_id)
);

CREATE TABLE IF NOT EXISTS user (
    user_id TEXT NOT NULL PRIMARY KEY UNIQUE,
    homeserver TEXT NOT NULL,
    identity_server TEXT NOT NULL,
    last_seen UNSIGNED INTEGER NOT NULL,
    avatar_url TEXT,
    display_name TEXT
);

CREATE TABLE IF NOT EXISTS account (
    user_id TEXT NOT NULL REFERENCES user(user_id) UNIQUE,
    scrypt_key TEXT NOT NULL
);

/**
CREATE TABLE account_config (
    user_id TEXT NOT NULL REFERENCES account(user_id),
);
*/

CREATE TABLE IF NOT EXISTS account_device (
    device_id TEXT NOT NULL,
    user_id TEXT NOT NULL REFERENCES user(user_id),
    last_seen UNSIGNED INTEGER NOT NULL,
    token TEXT NOT NULL UNIQUE,
    PRIMARY KEY(device_id, user_id, token)
);

/*
CREATE TABLE media (
);

CREATE TABLE homeserver (
    fqdn TEXT NOT NULL,
);

CREATE TABLE pdu (
    room_id TEXT PRIMARY NOT NULL,
    sender TEXT NOT NULL,
    origin TEXT NOT NULL,
    origin_server_ts TEXT,
    type
    state_key
    content
    prev_events
    depth
    auth_events
    redacts
    unsigned
    hashes
    signatures
);
*/

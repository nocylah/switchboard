insert into user values ("user0", "mat.neat.town", "id.neat.town", 0, "http://internet/cow.png", "User No Body");
insert into user values ("User1", "mat.cool.town", "id.cool.town", 0, "http://internet/fish.png", "User A Body");
insert into user values ("User2", "mat.neat.town", "id.neat.town", 0, "http://internet/dog.png", "User P Body");

insert into account values ("user0", "$rscrypt$0$AQEB$tRdfAUEtIDAlWc/dJ1zjvQ==$x0dPOHJzWlAtKFLdrwuKwvMYxFAvKp6N9TaSyfGII8M=$");
insert into account values ("User2", "$rscrypt$0$AQEB$tRdfAUEtIDAlWc/dJ1zjvQ==$x0dPOHJzWlAtKFLdrwuKwvMYxFAvKp6N9TaSyfGII8M=$");

insert into account_device values ("coolphone", "user0", 0, "ABCDEFG");
insert into account_device values ("coolcomputer", "user0", 0, "1234567");
